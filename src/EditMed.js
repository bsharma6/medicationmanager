import React from 'react';
import './App.css';
import Button from '@material-ui/core/Button';
import TextField from "@material-ui/core/TextField/TextField";
import Checkbox from "@material-ui/core/Checkbox/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel/FormControlLabel";


export default class EditMed extends React.Component {
    constructor( props ) {
        super( props );

        this.state = {
            ...this.props,
            monday: this.props.days.includes( "M" ),
            tuesday: this.props.days.includes( "Tu" ),
            wednesday: this.props.days.includes( "W" ),
            thursday: this.props.days.includes( "Th" ),
            friday: this.props.days.includes( "F" ),
            saturday: this.props.days.includes( "Sa" ),
            sunday: this.props.days.includes( "Su" )
        };
        this.nameChanged = this.nameChanged.bind( this );
        //this.captureInput = this.captureInput.bind( this );
        this.handleCheckboxChange = this.handleCheckboxChange.bind( this );
        this.timeChanged = this.timeChanged.bind( this );
        // this.deleteMed = this.deleteMed.bind( this );
        // this.editMed = this.editMed.bind( this );
        this.editInput = this.editInput.bind( this );
        // this.editName = this.editName.bind( this );
    }

    editInput() {
        let result ={
            "name": this.state.name,
            "days": this.formatDays(),
            "time": this.state.time,
            "_id": this.state._id
        };
        this.props.editInput( result );
    }
    componentDidMount() {

    }
    timeChanged( event ) {
        let value = event.target.value;
        this.setState( {
            time: value
        } );
    }

    handleCheckboxChange( event ) {
        let value = event.target.value;
        switch ( value ) {
            case "M":
                this.setState( {
                    monday: !this.state.monday
                } );
                break;
            case "Tu":
                this.setState( {
                    tuesday: !this.state.tuesday
                } );
                break;
            case "w":
                this.setState( {
                    wednesday: !this.state.wednesday
                } );
                break;
            case "th":
                this.setState( {
                    thursday: !this.state.thursday
                } );
                break;
            case "F":
                this.setState( {
                    friday: !this.state.friday
                } );
                break;
            case "sa":
                this.setState( {
                    saturday: !this.state.saturday
                } );
                break;
            case "su":
                this.setState( {
                    sunday: !this.state.sunday
                } );
                break;
        }
    }

    formatDays() {
        let days = "";
        for( let d in this.state ) {
            if ( d === "monday" || d === "tuesday" || d === "wednesday" || d === "thursday" || d === "friday"|| d === "saturday"|| d === "sunday" ) {
                if( this.state[ d ] ) {
                    if ( days !== "" ){
                        days = days+":";
                    }
                    switch ( d ) {
                        case "monday":
                            days = days + "M";
                            break;
                        case "tuesday":
                            days = days + "T";
                            break;
                        case "wednesday":
                            days = days + "W";
                            break;
                        case "thursday":
                            days = days + "Th";
                            break;
                        case "friday":
                            days = days + "Fr";
                            break;
                        case "saturday":
                            days = days + "Sa";
                            break;
                        case "sunday":
                            days = days + "Su";
                            break;
                    }
                }
            }
        }
        return days;
    }

    nameChanged( event ) {
        this.setState({
            name: event.target.value
        });
    }
    render() {
        return (
                 <form style={{
                     backgroundColor: 'white',
                     padding: '15px'
                 }}>
                     <TextField id="editName" label="Medicine Name" variant="outlined" style = { { marginRight: 10 } } onChange = { this.nameChanged } value={ this.state.name }/>
                          <div className="weekDays-selector">
                              <FormControlLabel
                                  control={
                                      <Checkbox
                                         checked={this.state.monday}
                                         onChange={this.handleCheckboxChange}
                                          value="M"
                                          color="primary"
                                         id = "editM"
                                          />
                                  }
                                  label="Mon"
                                   />
                                            <FormControlLabel
                                                control={
                                                    <Checkbox
                                                        checked={this.state.tuesday}
                                                        onChange={this.handleCheckboxChange}
                                                        value="Tu"
                                                        color="primary"
                                                        id = "editM"
                                                    />
                                                }
                                                label="Tue"
                                            />
                                            <FormControlLabel
                                                control={
                                                    <Checkbox
                                                        checked={this.state.wednesday}
                                                        onChange={this.handleCheckboxChange}
                                                        value="w"
                                                        color="primary"
                                                        id = "editM"
                                                    />
                                                }
                                                label="Wed"
                                            />
                                            <FormControlLabel
                                                control={
                                                    <Checkbox
                                                        checked={this.state.thursday}
                                                        onChange={this.handleCheckboxChange}
                                                        value="th"
                                                        color="primary"
                                                        id = "editM"
                                                    />
                                                }
                                                label="thu"
                                            />
                                            <FormControlLabel
                                                control={
                                                    <Checkbox
                                                        checked={this.state.friday}
                                                        onChange={this.handleCheckboxChange}
                                                        value="F"
                                                        color="primary"
                                                        id = "editM"
                                                    />
                                                }
                                                label="Fri"
                                            />
                                            <FormControlLabel
                                                control={
                                                    <Checkbox
                                                        checked={this.state.saturday}
                                                        onChange={this.handleCheckboxChange}
                                                        value="sa"
                                                        color="primary"
                                                        id = "editM"
                                                    />
                                                }
                                                label="Sat"
                                            />
                                            <FormControlLabel
                                                control={
                                                    <Checkbox
                                                        checked={this.state.sunday}
                                                        onChange={this.handleCheckboxChange}
                                                        value="su"
                                                        color="primary"
                                                        id = "editM"
                                                    />
                                                }
                                                label="Sun"
                                            />
                                        </div>
                                        <TextField
                                            id="editTime"
                                            label="Medicine Time"
                                            type="time"
                                            onChange={this.timeChanged}
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                            value = { this.state.time}
                                            inputProps={{
                                                step: 300, // 5 min
                                            }}
                                            style = { { marginRight: 10, color: '#0000008a' } }
                                        />
                                        <Button variant="contained" color="primary" onClick={ this.editInput } data-value = { this.state._id }>
                                            Save
                                        </Button>
                                    </form>
        );
    }
}
