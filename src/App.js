import React from 'react';
import './App.css';
import Button from '@material-ui/core/Button';
import { AppBar } from '@material-ui/core';
import Typography from "@material-ui/core/Typography/Typography";
import Toolbar from "@material-ui/core/Toolbar/Toolbar";
import TextField from "@material-ui/core/TextField/TextField";
import Container from "@material-ui/core/Container/Container";
import Checkbox from "@material-ui/core/Checkbox/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel/FormControlLabel";
import EditIcon from '@material-ui/icons/Edit';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import EditMed from "./EditMed";
import AssessmentIcon from '@material-ui/icons/Assessment';
import {AccountCircle} from "@material-ui/icons";
import EventIcon from '@material-ui/icons/Event';
export default class App extends React.Component {
    constructor( props ) {
        super( props );
        this.state = {
            medication : [],
            name: "",
            monday: false,
            tuesday: false,
            wednesday: false,
            thursday: false,
            friday: false,
            saturday: false,
            sunday: false,
            time: "07:30",
            username: {}
        };
        this.nameChanged = this.nameChanged.bind( this );
        this.captureInput = this.captureInput.bind( this );
        this.handleCheckboxChange = this.handleCheckboxChange.bind( this );
        this.timeChanged = this.timeChanged.bind( this );
        this.deleteMed = this.deleteMed.bind( this );
        this.editMed = this.editMed.bind( this );
        this.editInput = this.editInput.bind( this );
        this.fetchName = this.fetchName.bind( this );
        this.forwardAccount = this.forwardAccount.bind( this );
    }

    forwardAccount() {
        window.location.href = "https://medmind-account.herokuapp.com/accountInfo.html";
    }
    componentDidMount() {
        this.fetchName();
        this.fetchMed();
    }
    fetchName() {
        const requestOptions = {
            "async": true,
            "crossDomain": true,
            "url": "https://medmind-account.herokuapp.com/api/medmind/userDetails/bevans",
            "method": "GET",
            "headers": {
                "content-type": "application/json",
                "cache-control": "no-cache"
            }
        };
        fetch("https://medmind-account.herokuapp.com/api/medmind/userDetails/bevans", requestOptions).then( res =>  res.json() )
            .then(
                (result) => {
                    console.log( result );
                    this.setState({
                        username: result
                    });
                },
                (error) => {
                    console.log( error );
                }
            )
    }
    randomId() {
        // Math.random should be unique because of its seeding algorithm.
        // Convert it to base 36 (numbers + letters), and grab the first 9 characters
        // after the decimal. https://cors-anywhere.herokuapp.com/
        return '_' + Math.random().toString(36).substr(2, 9);
    };
    timeChanged( event ) {
        let value = event.target.value;
        this.setState( {
            time: value
        } );
    }
    editMed( event ) {
        let id= event.currentTarget.dataset.value;
        let med = this.state.medication.map( ( med ) => {
                if( med._id === id){
                    med.edit = true;
                }
            return med;
        } );
        // let med2 = [];
        // med2 = this.state.medication.map( ( med ) => {
        //     if( med._id !== id){
        //         return med;
        //     }
        // } );
        // med2 = med2.filter( Boolean );
        // med[0].edit = true;
        // let medication = [ ...med2, ...med ];
        // medication = medication.filter( Boolean );
        this.setState( {
            medication: med
        } );
    }

    handleCheckboxChange( event ) {
        let value = event.target.value;
        switch ( value ) {
            case "M":
                this.setState( {
                    monday: !this.state.monday
                } );
                break;
            case "Tu":
                this.setState( {
                    tuesday: !this.state.tuesday
                } );
                break;
            case "W":
                this.setState( {
                    wednesday: !this.state.wednesday
                } );
                break;
            case "Th":
                this.setState( {
                    thursday: !this.state.thursday
                } );
                break;
            case "F":
                this.setState( {
                    friday: !this.state.friday
                } );
                break;
            case "Sa":
                this.setState( {
                    saturday: !this.state.saturday
                } );
                break;
            case "Su":
                this.setState( {
                    sunday: !this.state.sunday
                } );
                break;
        }
    }

    formatDays() {
        let days = "";
        for( let d in this.state ) {
            if ( d === "monday" || d === "tuesday" || d === "wednesday" || d === "thursday" || d === "friday"|| d === "saturday"|| d === "sunday" ) {
                if( this.state[ d ] ) {
                    if ( days !== "" ){
                        days = days+":";
                    }
                    switch ( d ) {
                        case "monday":
                            days = days + "M";
                            break;
                        case "tuesday":
                            days = days + "Tu";
                            break;
                        case "wednesday":
                            days = days + "W";
                            break;
                        case "thursday":
                            days = days + "Th";
                            break;
                        case "friday":
                            days = days + "F";
                            break;
                        case "saturday":
                            days = days + "Sa";
                            break;
                        case "sunday":
                            days = days + "Su";
                            break;
                    }
                }
            }
        }
        return days;
    }

    editInput( body ){
        const requestOptions = {
            method: 'PUT',
            body: JSON.stringify( body ),
            headers: {
                'Content-Type': 'application/json'
            }
        };
        let prom = new Promise((resolve, reject) => {
            fetch("https://medmindmedication.herokuapp.com/api/medication/"+body._id,  requestOptions ).then(res => res.json())
                .then(
                    (result) => {
                        console.log( result );
                        resolve();
                    },
                    (error) => {
                        console.log( error );
                        reject();
                    }
                )
        });
        prom.then( () => {
            this.fetchMed();
        } );
    }
    fetchMed( ) {
        fetch("https://medmindmedication.herokuapp.com/api/medication")
            .then(res => res.json())
            .then(
                (result) => {
                    console.log( result );
                    this.setState({
                        medication: result.rows
                    });
                },
                (error) => {
                    console.log( error );
                }
            );
    }
    setMed( resolve, reject ) {
        let result ={
            "name": this.state.name,
            "days": this.formatDays(),
            "time": this.state.time,
            "_id": this.randomId()
        };
        const requestOptions = {
            method: 'POST',
            body: JSON.stringify( result ),
            headers: {
                'Content-Type': 'application/json'
            }
        };
        fetch("https://medmindmedication.herokuapp.com/api/medication/addMed", requestOptions )
            .then(res => res.json())
            .then(
                (result) => {
                    console.log( result );
                    resolve();
                },
                (error) => {
                    console.log( error );
                    reject();
                }
            );
    }
    deleteMed( event ) {
        let id= event.currentTarget.dataset.value;

        let prom = new Promise((resolve, reject) => {
            fetch("https://medmindmedication.herokuapp.com/api/medication/"+id,  {
                method: 'DELETE'
            } ).then(res => res.json())
                .then(
                    (result) => {
                        console.log( result );
                        resolve();
                    },
                    (error) => {
                        console.log( error );
                        reject();
                    }
                )
        });
        prom.then( () => {
            this.fetchMed();
        } );

    }
    nameChanged( event ) {
        this.setState({
           name: event.target.value
        });
    }

    captureInput ( event ) {
        event.preventDefault();
        let prom = new Promise((resolve, reject) => {
                this.setMed( resolve, reject );
        });
        prom.then( () => {
            this.fetchMed();
        } );
    }
    render() {

        return (
            <div>
            <AppBar position="static" style={ { width: "100%", display: "block", clear: "both" }} >
                <div style={ { display: "inline-block" }} >
                <Toolbar variant="dense">
                    <Typography variant="h5" color="inherit">
                       Medication manager
                    </Typography>
                </Toolbar>
                </div>

                <div style={ { display: "inline-block", float: "right" }}>
                    <Typography variant="h6" color="inherit" style={ { display: "inline-block", verticalAlign: "text-bottom" }}>
                        { this.state.username.firstname } { this.state.username.lastname }
                    </Typography>
                    <EventIcon  style={ { margin: "10px 10px", display: "inline-block", verticalAlign: "text-bottom" }}/>
                    <AssessmentIcon  style={ { margin: "10px 10px", display: "inline-block",verticalAlign: "text-bottom" }}/>
                    <AccountCircle  onClick = { this.forwardAccount } style={ { margin: "10px 10px", display: "inline-block", verticalAlign: "text-bottom" }}/>
                </div>
            </AppBar>
            <Container maxWidth="lg">
                <form style = { { marginTop: 30 } }>
                <TextField id="outlined-basic" label="Medicine Name" variant="outlined" style = { { marginRight: 10 } } onChange = { this.nameChanged }/>
                <div className="weekDays-selector">
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={this.state.monday}
                                onChange={this.handleCheckboxChange}
                                value="M"
                                color="primary"
                            />
                        }
                        label="Mon"
                    />
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={this.state.tuesday}
                                onChange={this.handleCheckboxChange}
                                value="Tu"
                                color="primary"
                            />
                        }
                        label="Tue"
                    />
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={this.state.wednesday}
                                onChange={this.handleCheckboxChange}
                                value="W"
                                color="primary"
                            />
                        }
                        label="Wed"
                    />
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={this.state.thursday}
                                onChange={this.handleCheckboxChange}
                                value="Th"
                                color="primary"
                            />
                        }
                        label="thu"
                    />
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={this.state.friday}
                                onChange={this.handleCheckboxChange}
                                value="F"
                                color="primary"
                            />
                        }
                        label="Fri"
                    />
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={this.state.saturday}
                                onChange={this.handleCheckboxChange}
                                value="Sa"
                                color="primary"
                            />
                        }
                        label="Sat"
                    />
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={this.state.sunday}
                                onChange={this.handleCheckboxChange}
                                value="Su"
                                color="primary"
                            />
                        }
                        label="Sun"
                    />
                </div>
                <TextField
                    id="time"
                    label="Medicine Time"
                    type="time"
                    onChange={this.timeChanged}
                    InputLabelProps={{
                        shrink: true,
                    }}
                    value = {this.state.time}
                    inputProps={{
                        step: 300, // 5 min
                    }}
                    style = { { marginRight: 10, color: '#0000008a' } }
                />
                    <Button variant="contained" color="primary" onClick={ this.captureInput }>
                        Submit
                    </Button>
                </form>
                <ul className="med-list-items MuiTypography-h6">
                    {this.state.medication.map((meds) => {
                        return <li className="med-list-item" key={meds._id}>
                            {  meds.edit ?  <EditMed { ...meds } editInput = { this.editInput }/>:
                            <div>
                            <span style = {{ width: "30%", display: "inline-block" }}>{meds.name}</span>
                            <span style = {{ width: "30%", display: "inline-block" }}>{meds.days.split( ":" ).map( ( d, i ) => {
                               return <span className="med-list-days" key={ i }> {d} </span>;
                            })}</span>
                            <span>{meds.time}</span>
                            <span style={{ float: "right", margin: "0px 30px" }}>
                            <EditIcon style={{ margin: "0px 10px", cursor: "pointer"  }} onClick = { this.editMed } key = { "edit"+meds._id } data-value = { meds._id }/>
                            <DeleteForeverIcon style={{ margin: "0px 10px", cursor: "pointer" }} onClick = { this.deleteMed } key = { "delete"+meds._id } data-value = { meds._id }/>
                            </span>
                            </div> }
                        </li>;
                    })}

                </ul>
            </Container>
            </div>
        );
    }
}
